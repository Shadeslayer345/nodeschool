var fs = require('fs');
var path = require('path');

/**
 * Returns a list of files within a directory filtered by a user specified
 * filetype.
 * 
 * @param  {!string} path The name of the directory to search.
 * @param {!string} folder File type extension to filter for.
 * @param {function} finishCall The callback function that will process the
 *    return on the function.
 * @return {error} Will return the error message if unsuccessful.
 */
module.exports = function(folder, ext, finishCall) {
	return fs.readdir(folder, function(error, dir) {
		if (error) {
			return finishCall(error,null);	
		}
		dir = dir.filter(function(file) {
			return path.extname(file) == '.' + ext;
		});
		return finishCall(null, dir);
	});
};