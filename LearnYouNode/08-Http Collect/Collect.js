var http = require('http');

http.get(process.argv[2], function(response) {
	var charCount = 0;
	var Output = '';
	response.setEncoding('utf8');
	response.on('error', function(error) {
		console.log('error connecting to address. ' + error.message);
	}).on('data', function(data) {
		charCount += Number(data.length);
		Output += data;
	}).on('end', function() {
		console.log(charCount);
		console.log(Output);
	});
});