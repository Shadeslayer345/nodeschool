var http = require('http');
var portNumber = process.argv[2];
var fs = require('fs');
var fileServer = http.createServer(function(request, response) {
  response.writeHead(200, {'Content-Type': 'text/plain'});
  fs.createReadStream(process.argv[3], {encoding: 'utf8'}).pipe(response);
});
fileServer.listen(portNumber);
