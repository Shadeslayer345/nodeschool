var http = require('http');
var map = require('through2-map');
var portNumber = process.argv[2];

var fileServer = http.createServer(function(request, response) {
  //TODO add check for type (POST or nonPOST)
  request.pipe(map(function(chunk) {
    return chunk.toString().toUpperCase();
  })).pipe(response);
});
fileServer.listen(portNumber);


