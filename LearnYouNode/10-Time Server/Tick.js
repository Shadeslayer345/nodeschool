var backend = require('net');
var portNumber = process.argv[2];
var tcpServer = backend.createServer(function(socket) {
  var date = new Date();
  var year = date.getFullYear().toString();
  var month = date.getMonth() + 1;
  if (month.toString().length == 1) {
    month = '0' + month.toString();
  }
  var day = date.getDate().toString();
  if (day.length == 1) {
    day = '0' + day;
  }
  var hours = date.getHours().toString();
  if (hours.length == 1) {
    hours = '0' + hours;
  }
  var minutes = date.getMinutes().toString();
  if (minutes.length == 1) {
    minutes = '0' + minutes;
  }
  var frmtme =
      year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + '\n';
  socket.end(frmtme);
});
tcpServer.listen(portNumber);
