var fs = require('fs');
var path = require('path');
var type = '.'+ process.argv[3];

fs.readdir(process.argv[2], function(error, dir) {
	if (error) {
		return error.message;
	} 
	dir.forEach(function(file) {
		if (path.extname(file) == type) {
			console.log(file);
		}
	});
});
